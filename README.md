# HelloWorld-DotNet



# Table of contents 
- [HelloWorld-DotNet](#helloworld-dotnet)
- [Table of contents](#table-of-contents)
  - [Steps:](#steps)
    - [1. Solution File AppNet <a name="Solution_File_AppNet"></a>](#1-solution-file-appnet-)
    - [2. Create folders.](#2-create-folders)
    - [3. New Console HelloWorld.](#3-new-console-helloworld)
    - [4. Add src/HelloWorld](#4-add-srchelloworld)
    - [5. Test (xunit).](#5-test-xunit)
    - [6. Add HelloWorldTests](#6-add-helloworldtests)
    - [7. Reference HelloWorld](#7-reference-helloworld)
    - [8. Add package FsUnit](#8-add-package-fsunit)
    - [9. Add package FsUnit.XUnit](#9-add-package-fsunitxunit)
    - [10. Build](#10-build)
    - [11. Test](#11-test)


## Steps:
### 1. Solution File AppNet <a name="Solution_File_AppNet"></a>
  - [X] `docker run --rm -ti -v $(pwd):/app -w /app/ mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet new sln -o AppNet'`

```
The template "Solution File" was created successfully.
```
### 2. Create folders.
`sudo mkdir AppNet/src AppNet/tests`

```.
└── AppNet
    ├── src
    └── tests
```
### 3. New Console HelloWorld.    
`docker run --rm -ti -v $(pwd):/app -w /app/AppNet mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet new console -lang F# -o src/HelloWorld'`

```
The template "Console App" was created successfully.

Processing post-creation actions...
Running 'dotnet restore' on /app/AppNet/src/HelloWorld/HelloWorld.fsproj...
  Determining projects to restore...
  Restored /app/AppNet/src/HelloWorld/HelloWorld.fsproj (in 329 ms).
Restore succeeded.
```

### 4. Add src/HelloWorld
`docker run --rm -ti -v $(pwd):/app -w /app/AppNet mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet sln add src/HelloWorld/HelloWorld.fsproj'`
```
Project `src/HelloWorld/HelloWorld.fsproj` added to the solution.
```

### 5. Test (xunit).

`docker run --rm -ti -v $(pwd):/app -w /app/AppNet mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet new xunit -lang F# -o tests/HelloWorldTests'`
```
The template "xUnit Test Project" was created successfully.

Processing post-creation actions...
Running 'dotnet restore' on /app/AppNet/tests/HelloWorldTests/HelloWorldTests.fsproj...
  Determining projects to restore...
  Restored /app/AppNet/tests/HelloWorldTests/HelloWorldTests.fsproj (in 12.21 sec).
Restore succeeded.
```
### 6. Add HelloWorldTests
`docker run --rm -ti -v $(pwd):/app -w /app/AppNet mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet sln add tests/HelloWorldTests/HelloWorldTests.fsproj'`
```
Project `tests/HelloWorldTests/HelloWorldTests.fsproj` added to the solution.
```
### 7. Reference HelloWorld

`docker run --rm -ti -v $(pwd):/app -w /app/AppNet/tests/HelloWorldTests mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet add reference ../../src/HelloWorld/HelloWorld.fsproj'`
```
Reference `..\..\src\HelloWorld\HelloWorld.fsproj` added to the project.
```

### 8. Add package FsUnit

`docker run --rm -ti -v $(pwd):/app -w /app/AppNet/tests/HelloWorldTests mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet add package FsUnit'`
```
  Determining projects to restore...
  Writing /tmp/tmpSxU6xH.tmp
info : Adding PackageReference for package 'FsUnit' into project '/app/AppNet/tests/HelloWorldTests/HelloWorldTests.fsproj'.
info :   GET https://api.nuget.org/v3/registration5-gz-semver2/fsunit/index.json
info :   OK https://api.nuget.org/v3/registration5-gz-semver2/fsunit/index.json 739ms
.........
.......
....
```
### 9. Add package FsUnit.XUnit

`docker run --rm -ti -v $(pwd):/app -w /app/AppNet/tests/HelloWorldTests mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet add package FsUnit.XUnit'`
```
  Determining projects to restore...
  Writing /tmp/tmpgONhPJ.tmp
info : Adding PackageReference for package 'FsUnit.XUnit' into project '/app/AppNet/tests/HelloWorldTests/HelloWorldTests.fsproj'.
info :   GET https://api.nuget.org/v3/registration5-gz-semver2/fsunit.xunit/index.json
........
.......
```
### 10. Build

`docker run --rm -ti -v $(pwd):/app -w /app/AppNet/tests/HelloWorldTests mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet build'`
```
Microsoft (R) Build Engine version 17.2.0+41abc5629 for .NET
Copyright (C) Microsoft Corporation. All rights reserved.

  Determining projects to restore...
  Restored /app/AppNet/src/HelloWorld/HelloWorld.fsproj (in 393 ms).
  Restored /app/AppNet/tests/HelloWorldTests/HelloWorldTests.fsproj (in 9.48 sec).
  HelloWorld -> /app/AppNet/src/HelloWorld/bin/Debug/net6.0/HelloWorld.dll
  HelloWorldTests -> /app/AppNet/tests/HelloWorldTests/bin/Debug/net6.0/HelloWorldTests.dll

Build succeeded.
    0 Warning(s)
    0 Error(s)

Time Elapsed 00:00:14.20
```

### 11. Test
$ docker run --rm -ti -v $(pwd):/app -w /app/AppNet/tests/HelloWorldTests mcr.microsoft.com/dotnet/sdk:6.0 sh -c 'dotnet test'
```
  Determining projects to restore...
  Restored /app/AppNet/src/HelloWorld/HelloWorld.fsproj (in 395 ms).
  Restored /app/AppNet/tests/HelloWorldTests/HelloWorldTests.fsproj (in 10.78 sec).
  HelloWorld -> /app/AppNet/src/HelloWorld/bin/Debug/net6.0/HelloWorld.dll
  HelloWorldTests -> /app/AppNet/tests/HelloWorldTests/bin/Debug/net6.0/HelloWorldTests.dll
Test run for /app/AppNet/tests/HelloWorldTests/bin/Debug/net6.0/HelloWorldTests.dll (.NETCoreApp,Version=v6.0)
Microsoft (R) Test Execution Command Line Tool Version 17.2.0 (x64)
Copyright (c) Microsoft Corporation.  All rights reserved.

Starting test execution, please wait...
A total of 1 test files matched the specified pattern.

Passed!  - Failed:     0, Passed:     1, Skipped:     0, Total:     1, Duration: < 1 ms - /app/AppNet/tests/HelloWorldTests/bin/Debug/net6.0/HelloWorldTests.dll (net6.0)
```
