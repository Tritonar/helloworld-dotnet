FROM mcr.microsoft.com/dotnet/sdk:6.0
WORKDIR /app 
RUN dotnet new sln -o AppNet
WORKDIR /app/AppNet
RUN mkdir src tests
RUN dotnet new console -lang F# -o src/HelloWorld
RUN dotnet sln add src/HelloWorld/HelloWorld.fsproj
RUN dotnet new xunit -lang F# -o tests/HelloWorldTests
RUN dotnet sln add tests/HelloWorldTests/HelloWorldTests.fsproj
WORKDIR /app/AppNet/tests/HelloWorldTests
RUN dotnet add reference ../../src/HelloWorld/HelloWorld.fsproj
RUN dotnet add package FsUnit
RUN dotnet add package FsUnit.XUnit
RUN dotnet build
RUN dotnet test
